from PIL import Image, ImageFilter
import numpy as np
import cv2

# Load the image from file
file_path = '/Users/dakotagoldberg/cba/raspi_drawing_machine/code/input/test.jpg'
image = cv2.imread(file_path)

# Convert the image to RGB (OpenCV uses BGR by default)
image_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

# Use haarcascade_frontalface_default.xml to detect faces
face_cascade = cv2.CascadeClassifier(
    cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')

# Detect faces in the image
faces = face_cascade.detectMultiScale(
    image_rgb, scaleFactor=1.8, minNeighbors=5, minSize=(50, 50))

# Create a mask where the faces are 0 and the rest is 1
mask = np.ones_like(image) * 255  # White mask
for (x, y, w, h) in faces:
    mask[y:y+h, x:x+w] = 0  # Set the face region to black

# Convert mask to boolean
mask_bool = mask[:, :, 0] == 0

image[~mask_bool] = 255  # Set the face region to white


# Create a blurred version of the original image
blurred_image = cv2.GaussianBlur(image, (25, 25), 0)

# Copy the clear face region from the original image
final_image = image.copy()
final_image[~mask_bool] = blurred_image[~mask_bool]

# Convert the final image to PIL format for saving
final_image_pil = Image.fromarray(cv2.cvtColor(image, cv2.COLOR_BGR2RGB))

# Save the final image
output_path = '/Users/dakotagoldberg/cba/raspi_drawing_machine/code/output/result.jpg'
final_image_pil.save(output_path)
