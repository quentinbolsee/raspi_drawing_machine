from pathlib import Path
from bf import bf
from path import path_ordering
from stroke import stroke
from vec import convert_to_vector
import os

path = Path(__file__).parent.absolute()


def main(input_path, output_path, num):

    tmp_path = f'{path}/tmp/tmp.png'
    tmp_svg = f'{path}/tmp/tmp.svg'

    bf(input_path, tmp_path)
    stroke(tmp_path, tmp_svg)
    path_ordering(tmp_svg, f"{output_path}/{num}.json")


if __name__ == "__main__":
    input_path = f'{path}/input'
    output_path = f'{path}/output'

    for i, filename in enumerate(os.listdir(input_path)):
        filepath = os.path.join(
            input_path, filename)
        if os.path.isfile(filepath):
            print(filename)

            main(f'{input_path}/{filename}',
                 f'{output_path}', i)
