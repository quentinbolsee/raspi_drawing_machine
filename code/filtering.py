import cv2
import numpy as np
import matplotlib.pyplot as plt
from skimage import morphology, measure
import trace_skeleton
import random


def bf(img):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    filtered = cv2.bilateralFilter(gray, d=17, sigmaColor=75, sigmaSpace=75)
    # filtered = cv2.Sobel(filtered, ddepth=cv2.CV_8U, dx=1, dy=1, ksize=5)
    # gray = cv2.cvtColor(filtered, cv2.COLOR_BGR2GRAY)
    edges = cv2.adaptiveThreshold(filtered, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, blockSize=9, C=4)
    # post_filtered = cv2.bilateralFilter(edges, d=9, sigmaColor=75, sigmaSpace=75)
    edges = ~edges

    edges = cv2.dilate(edges, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5)))

    # return post_filtered
    return edges


# def stroke(img):
#     img_float = img.astype(np.float32) / 255.0
#     img_binary = img_float < 0.5
#     out_thin = morphology.thin(img_binary)
#     out_skeletonize = morphology.skeletonize(img_binary)
#     plt.figure()
#     plt.imshow(out_skeletonize)
#     plt.show()
#
#     contours = measure.find_contours(out_thin, 0.7)
#     return contours


def stroke(img):
    img_copy = np.zeros_like(img)
    img2 = bf(img)
    img_float = img2.astype(np.float32) / 255.0
    img_binary = img_float > 0.5

    im = trace_skeleton.thinning(img_binary)

    plt.figure()

    rects = []
    polys = trace_skeleton.traceSkeleton(im, 0, 0, im.shape[1], im.shape[0], 10, 999, rects)
    for l in polys:
        c = (200 * random.random(), 200 * random.random(), 200 * random.random())
        for i in range(0, len(l) - 1):
            cv2.line(img_copy, (l[i][0], l[i][1]), (l[i + 1][0], l[i + 1][1]), c)
    return img_copy


if __name__ == "__main__":
    # img = cv2.imread("old/test.png", cv2.IMREAD_UNCHANGED)
    # img = img[:, :, :3]
    # img2 = bf(img)
    # contours = stroke(img2)
    # plt.figure()
    # for cnt in contours:
    #     plt.plot(cnt[:, 0], cnt[:, 1])
    # plt.show()
    # # print(contours[0])
    img = cv2.imread("old/test.png", cv2.IMREAD_UNCHANGED)
    img = img[:, :, :3]
    # img_copy = np.copy(cv2.cvtColor(img, cv2.COLOR_BGR2GRAY))*0
    img_copy = np.zeros_like(img)

    img2 = bf(img)
    img_float = img2.astype(np.float32) / 255.0
    img_binary = img_float > 0.5

    im = trace_skeleton.thinning(img_binary)

    plt.figure()

    rects = []
    polys = trace_skeleton.traceSkeleton(im, 0, 0, im.shape[1], im.shape[0], 10, 999, rects)
    for l in polys:
        c = (200 * random.random(), 200 * random.random(), 200 * random.random())
        for i in range(0, len(l) - 1):
            cv2.line(img_copy, (l[i][0], l[i][1]), (l[i + 1][0], l[i + 1][1]), c)
            # plt.plot((l[i][0], l[i + 1][0]), (l[i][1], l[i + 1][1]))
    # plt.axis("equal")

    # plt.imshow(img_copy)
    # plt.show()

    # plt.figure()
    # plt.imshow(im)
    # plt.show()
    cv2.imshow('', img_copy)
    cv2.waitKey(0)
