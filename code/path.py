import xml.etree.ElementTree as ET
from svgoutline import svg_to_outlines
import json


def path_ordering(input_path, output_path):
    tree = ET.parse(input_path)
    root = tree.getroot()
    outlines = svg_to_outlines(
        root, width_mm=140, height_mm=140)
    with open(output_path, 'w') as f:
        json.dump(outlines, f)


# pixels_per_mm=5.0
