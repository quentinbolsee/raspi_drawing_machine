import cv2
import numpy as np
import os


def bf(image_path, output_path):

    image = cv2.imread(image_path)

    filtered = cv2.bilateralFilter(image, d=9, sigmaColor=75, sigmaSpace=75)

    gray = cv2.cvtColor(filtered, cv2.COLOR_BGR2GRAY)

    edges = cv2.adaptiveThreshold(
        gray, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, blockSize=9, C=4)

    # inverted_edges = cv2.bitwise_not(edges)

    post_filtered = cv2.bilateralFilter(
        edges, d=9, sigmaColor=75, sigmaSpace=75)

    # Save the result
    cv2.imwrite(output_path, post_filtered)


# for filename in os.listdir('/Users/dakotagoldberg/cba/raspi_drawing_machine/code/img/input-resized'):
#     filepath = os.path.join(
#         '/Users/dakotagoldberg/cba/raspi_drawing_machine/code/img/input-resized', filename)
#     # Check if it is a file
#     if os.path.isfile(filepath):
#         print(filename)

#         # name = "polite_cat.png"
#         bf(f'/Users/dakotagoldberg/cba/raspi_drawing_machine/code/img/input-resized/{filename}',
#            f'/Users/dakotagoldberg/cba/raspi_drawing_machine/code/output2/{filename}')
