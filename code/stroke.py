import cv2
import numpy as np
import os
from skimage import img_as_float, io, color, morphology, measure
import matplotlib.pyplot as plt
import numpy as np
from PIL import Image
from skimage import measure
from xml.dom.minidom import getDOMImplementation


def create_svg_path(contour):
    """
    Create an SVG path string from a contour.
    """

    d = "M {} {}".format(contour[0][1], contour[0][0])

    for (y, x) in contour[1:]:
        d += " L {} {}".format(x, y)

    d += " Z"
    return d


def contours_to_svg(contours, width, height):
    """
    Convert a list of contours to an SVG document.
    """
    impl = getDOMImplementation()

    svg_doc = impl.createDocument(None, "svg", None)
    svg_root = svg_doc.documentElement
    svg_root.setAttribute("width", str(width))
    svg_root.setAttribute("height", str(height))
    svg_root.setAttribute("xmlns", "http://www.w3.org/2000/svg")

    g_element = svg_doc.createElement("g")
    g_element.setAttribute("fill", "none")
    g_element.setAttribute("stroke", "black")
    g_element.setAttribute("stroke-width", "1")

    for contour in contours:
        path_data = create_svg_path(contour)
        path_element = svg_doc.createElement("path")
        path_element.setAttribute("d", path_data)
        g_element.appendChild(path_element)

    svg_root.appendChild(g_element)
    return svg_doc.toxml()


def stroke(image_path, output_path):

    image = img_as_float((io.imread(image_path)))
    image_binary = image < 0.5
    out_skeletonize = morphology.skeletonize(image_binary)
    out_thin = morphology.thin(image_binary)
    # threshold_value = 128
    # binary_image = image_np < threshold_value

    contours = measure.find_contours(out_thin, 0.7)

    image_width, image_height = out_thin.shape[::-1]

    svg_data = contours_to_svg(contours, image_width, image_height)

    svg_file_path = output_path
    with open(svg_file_path, 'w') as svg_file:
        svg_file.write(svg_data)

    # fig, ax = plt.subplots()
    # ax.imshow(255 - out_thin, cmap='gray')
    # ax.axis('off')  # Turn off axis

    # fig.savefig(output_path, bbox_inches='tight', pad_inches=0, dpi=100)
    # plt.close(fig)


input_path = '/Users/dakotagoldberg/cba/raspi_drawing_machine/code/output'
output_path = '/Users/dakotagoldberg/cba/raspi_drawing_machine/code/stroke'


# for filename in os.listdir(input_path):
#     filepath = os.path.join(
#         input_path, filename)
#     # Check if it is a file
#     if os.path.isfile(filepath):
#         print(filename)

#         # name = "polite_cat.png"
#         stroke(f'{input_path}/{filename}',
#                f'{output_path}/{filename}')
