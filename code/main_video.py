import numpy as np
import cv2
import tqdm
import argparse


def parse_arguments():
    usage_text = (
        "Usage:  python crop_svg.py [options]"
    )
    parser = argparse.ArgumentParser(description=usage_text)
    parser.add_argument("-i", "--input", type=str, required=True, help="Input npy file.")
    parser.add_argument("-s", "--slice", type=int, required=True, help="Slice.")
    parser.add_argument("-o", "--output", type=str, required=True, help="Input host file.")
    return parser.parse_known_args()


def main():
    filename_in = "img/pictured_cat.png"
    filename_out = "test.mp4"

    vid_writer = None

    img = cv2.imread(filename_in, cv2.IMREAD_UNCHANGED)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    h, w = img.shape

    threshold_values = np.arange(200)

    for i in tqdm.tqdm(range(len(threshold_values))):
        thresh = threshold_values[i]
        if vid_writer is None:
            fourcc = cv2.VideoWriter_fourcc(*'mp4v')
            vid_writer = cv2.VideoWriter(filename_out, fourcc, 60, (w, h))

        edges = cv2.Canny(img, thresh//2, thresh)
        vid_writer.write(cv2.cvtColor(edges, cv2.COLOR_GRAY2BGR))

    vid_writer.release()


if __name__ == "__main__":
    main()
