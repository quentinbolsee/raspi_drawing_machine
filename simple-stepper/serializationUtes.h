#ifndef SERIALIZATION_UTES_H_
#define SERIALIZATION_UTES_H_

#include <Arduino.h>

union chunk_float32 {
  uint8_t bytes[4];
  float f;
};

float ts_readFloat32(unsigned char* buf, uint16_t* ptr);

void ts_writeFloat32(float val, volatile unsigned char* buf, uint16_t* ptr);

#endif 