#include "COBSUSBSerial.h"
#include "serializationUtes.h"
#include "motionStateMachine.h"
#include "stepperDriver.h"

// no-transport-layer stepper motor 
// using the RP2040 at 200MHz 
// using https://modular-things.github.io/modular-things/things/stepper-hbridge-xiao/

#define PIN_LIMIT 26 

#define MSG_GET_ID        7
#define MSG_ECHO_TEST     11 
#define MSG_SET_TARG_VEL  21
#define MSG_SET_TARG_POS  22
#define MSG_SET_POSITION  23 
#define MSG_SET_CURRENT   24
#define MSG_GET_STATES    25
#define MSG_ACK           31 
#define MSG_NACK          32

// y_motor: 1, x_bottom_motor: 2, x_top_motor: 3 
#define MOTOR_ID 3

COBSUSBSerial cobs(&Serial);


void setTargetVelocity(uint8_t* data, size_t len){
  uint16_t wptr = 0;
  float targ = ts_readFloat32(data, &wptr);
  float maxAccel = ts_readFloat32(data, &wptr);
  motion_setVelocityTarget(targ, maxAccel);
}


void setTargetPosition(uint8_t* data, size_t len){
  uint16_t wptr = 0;
  float targ = ts_readFloat32(data, &wptr);
  float maxVel = ts_readFloat32(data, &wptr);
  float maxAccel = ts_readFloat32(data, &wptr);
  motion_setPositionTarget(targ, maxVel, maxAccel);
}


void setPosition(uint8_t* data, size_t len){
  // should do maxAccel, maxVel, and (optionally) setPosition
  // upstream should've though of this, so,
  uint16_t rptr = 0;
  float pos = ts_readFloat32(data, &rptr);
  motion_setPosition(pos);
}


void setCurrent(uint8_t* data, size_t len){
  // it's just <cscale> for the time being,
  uint16_t rptr = 0;
  float cscale = ts_readFloat32(data, &rptr);
  // we get that as a floating p, 0-1, 
  // driver wants integers 0-1024: 
  uint32_t amp = cscale * 1024;
  stepper_setAmplitude(amp);
}


boolean lastButtonState = false;

size_t getStates(uint8_t* data, size_t len, uint8_t* reply){
  motionState_t state;
  motion_getCurrentStates(&state);
  uint16_t wptr = 0;
  // in-fill current posn, velocity, and acceleration
  ts_writeFloat32(state.pos, reply, &wptr);
  ts_writeFloat32(state.vel, reply, &wptr);
  ts_writeFloat32(state.accel, reply, &wptr);
  // and limit state, 
  reply[wptr ++] = (lastButtonState ? 1 : 0);
  // return the data length 
  return wptr;
}


void setup() {
  // startup the stepper hardware 
  stepper_init();
  // setup motion, pick an integration interval (us) 
  motion_init(64);
  // and our limit pin, is wired (to spec)
  // to a normally-closed switch, from SIG to GND, 
  // meaning that when the switch is "clicked" - it will open, 
  // and the pullup will win, we will have logic high 
  pinMode(PIN_LIMIT, INPUT_PULLUP);
  // and our comms
  cobs.begin();
}


uint32_t debounceDelay = 1;
uint32_t lastButtonCheck = 0;

motionState_t states;

uint8_t msg[256];
uint8_t reply[256];

void loop() {
  // run comms
  cobs.loop();
  // and check for paquiats: condition is if we can 
  // get a packet, and tx the reply
  if(cobs.clearToRead() && cobs.clearToSend()){
    size_t len = cobs.getPacket(msg);
    // start a reply, 
    size_t wptr = 0;
    reply[wptr ++] = MSG_ACK;
    // switch on grams,
    switch(msg[0]){
      case MSG_GET_ID:
        reply[wptr ++] = MOTOR_ID;
        break;
      case MSG_ECHO_TEST:
        memcpy(reply, msg, len);
        wptr = len;
        break;
      case MSG_SET_TARG_VEL:
        setTargetVelocity(msg + 1, len - 1);
        break;
      case MSG_SET_TARG_POS:
        setTargetPosition(msg + 1, len - 1);
        break;
      case MSG_SET_POSITION:
        setPosition(msg + 1, len - 1);
        break;
      case MSG_SET_CURRENT:
        setCurrent(msg + 1, len - 1);
        break;
      case MSG_GET_STATES:
        wptr += getStates(msg + 1, len - 1, reply + 1);
        break;
      default:
        reply[0] = MSG_NACK;
        break;
    }
    // send the reply, 
    cobs.send(reply, wptr);
  }
  // debounce and set button states,
  if(lastButtonCheck + debounceDelay < millis()){
    lastButtonCheck = millis();
    boolean newState = digitalRead(PIN_LIMIT);
    if(newState != lastButtonState){
      lastButtonState = newState;
    }
  }
}
