from motor import Motor, HBridge
import time

solenoid = HBridge("COM31")

for i in range(10):
    print(i)
    solenoid.set_power(0.45, 1)
    time.sleep(0.2)
    solenoid.set_power(0.0, 1)
    time.sleep(0.2)
