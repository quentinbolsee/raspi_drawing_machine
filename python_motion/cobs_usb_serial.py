from cobs import cobs
import serial


class CobsUsbSerial:
    def __init__(self, port, baudrate=115200):
        self.port = port
        self.baudrate = baudrate
        self.ser = serial.Serial(port, baudrate=baudrate)

    def write(self, data: bytes):
        data_enc = cobs.encode(data) + b"\x00"
        self.ser.write(data_enc)

    def read(self):
        data_enc = self.ser.read_until(b"\x00")
        data = cobs.decode(data_enc[:-1])
        return data
