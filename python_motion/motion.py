from motor import Motor, HBridge
import random
import time

home_rate = 80
home_accel = 2000
home_backoff_time = 0.2

go_rate = 150
go_accel = 2000

use_current = 0.4

solenoid = HBridge("COM31")
y_motor = Motor("COM32")
x_bottom_motor = Motor("COM36")
x_top_motor = Motor("COM22")

def init():
    # steps / unit,
    y_motor.set_steps_per_unit(200 / 40)
    x_bottom_motor.set_steps_per_unit(200 / 40)
    x_top_motor.set_steps_per_unit(200 / 40)

    # power up
    y_motor.set_current(use_current)
    x_bottom_motor.set_current(use_current)
    x_top_motor.set_current(use_current)


def pen_action(index=0, down=False, delay=0.04):
    global solenoid
    if index == 2:
        solenoid.set_power(0.45 if down else 0.0, 0)
        solenoid.set_power(0.45 if down else 0.0, 1)
    else:
        solenoid.set_power(0.45 if down else 0.0, index)
    time.sleep(delay)


def homing():
    # home Y ....
    # if we're on the switch, backoff:
    if y_motor.get_states()['limit'] > 0:
        y_motor.set_target_velocity(home_rate, home_accel)
        time.sleep(home_backoff_time)
        y_motor.set_target_velocity(0, home_accel)
        time.sleep(home_backoff_time)

    # let's home it,
    y_motor.set_target_velocity(-home_rate, home_accel)

    # then check-wait on states,
    while True:
        states = y_motor.get_states()
        if states['limit'] > 0:
            break

    y_motor.set_target_velocity(0, home_accel)

    # home X ...
    if x_bottom_motor.get_states()['limit'] > 0:
        x_bottom_motor.set_target_velocity(home_rate, home_accel)
        x_top_motor.set_target_velocity(home_rate, home_accel)
        time.sleep(home_backoff_time)
        x_bottom_motor.set_target_velocity(0, home_accel)
        x_top_motor.set_target_velocity(0, home_accel)
        time.sleep(home_backoff_time)

    x_bottom_motor.set_target_velocity(-home_rate, home_accel)
    x_top_motor.set_target_velocity(-home_rate, home_accel)

    while True:
        states = x_bottom_motor.get_states()
        if states['limit'] > 0:
            break

    x_bottom_motor.set_target_velocity(0, home_accel)
    x_top_motor.set_target_velocity(0, home_accel)
    time.sleep(home_backoff_time)

    # set zeroes,
    x_bottom_motor.set_position(0)
    x_top_motor.set_position(0)
    y_motor.set_position(0)

# goto points 
def goto(x, y, vel, accel, eps=0.5):
    x_bottom_motor.set_target_position(x, vel, accel)
    x_top_motor.set_target_position(x, vel, accel)
    y_motor.set_target_position(y, vel, accel)
    # ... wait while not at point:
    while True:
        x_states = x_top_motor.get_states()
        y_states = y_motor.get_states()
        x_err = abs(x_states["position"] - x)
        y_err = abs(y_states["position"] - y)
        if x_err <= eps and y_err <= eps:
            break


import numpy as np
import math


def do_circle():
    theta = np.linspace(0, 2*math.pi, 16)
    x = 50 + 20*np.cos(theta)
    y = 130 + 20*np.sin(theta)
    for i in range(len(x)):
        goto(x[i], y[i], go_rate, go_accel, eps=0.5)
        if i % 2 == 0:
            pen_action(0, True)
        else:
            pen_action(0, False)


def do_wave():
    theta = np.linspace(0, 6*math.pi, 64)
    x = 80 + theta * 5
    y = 130 + 10*np.sin(theta)
    for i in range(len(x)):
        goto(x[i], y[i], go_rate, go_accel, eps=0.5)


origin = (130, 118)
init()
homing()
goto(0, 118, go_rate, go_accel)
goto(130, 118, go_rate, go_accel)
pen_action(2, True)
pen_action(2, False)
goto(0, 118, go_rate, go_accel)
# goto(80, 130, go_rate, go_accel)
# pen_action(2, True)
# # pen_action(1, True)
# # do_circle()
# do_wave()
# pen_action(2, False)
# # pen_action(1, False)
# goto(0, 130, go_rate, go_accel)
# goto(0, 50, go_rate, go_accel)

# goto(30, 100, go_rate, go_accel)
# goto(50, 100, go_rate, go_accel)
# goto(50, 120, go_rate, go_accel)
# goto(30, 120, go_rate, go_accel)
# goto(30, 100, go_rate, go_accel)
# pen_action(1, False)

# time.sleep(2)
# goto(200, 100, go_rate, go_accel)
# time.sleep(2)
# goto(100, 100, go_rate, go_accel)
# time.sleep(2)
# goto(100, 50, go_rate, go_accel)
