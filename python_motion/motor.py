from cobs_usb_serial import CobsUsbSerial
import struct

MSG_GET_ID = 7
MSG_ECHO_TEST = 11
MSG_SET_POWER = 20
MSG_SET_TARG_VEL = 21
MSG_SET_TARG_POS = 22
MSG_SET_POSITION = 23
MSG_SET_CURRENT = 24
MSG_GET_STATES = 25
MSG_ACK = 31
MSG_NACK = 32


class Motor:
    def __init__(self, port: str):
        self.cobs = CobsUsbSerial(port)
        self.spu = 200

    def get_id(self):
        msg = struct.pack('=B', MSG_GET_ID)
        self.cobs.write(msg)
        ack, device_id = struct.unpack('=BB', self.cobs.read())
        return device_id

    def set_steps_per_unit(self, spu):
        self.spu = spu

    def test(self) -> None:
        test_bytes = bytes([MSG_ECHO_TEST, 14, 16])
        print("testing w/ an echo-packet: ", [byte for byte in test_bytes])
        self.cobs.write(test_bytes)
        reply = self.cobs.read()
        print("test echo returns: ", [byte for byte in reply])

    def set_target_velocity(self, velocity: float, accel: float):
        msg = struct.pack('=Bff', MSG_SET_TARG_VEL, velocity * self.spu, accel * self.spu)
        self.cobs.write(msg)
        return struct.unpack('=B', self.cobs.read())

    def set_target_position_no_wait(self, position: float, velocity: float, accel: float):
        msg = struct.pack('=Bfff', MSG_SET_TARG_POS, position * self.spu, velocity * self.spu, accel * self.spu)
        self.cobs.write(msg)
        return

    def set_target_position(self, position: float, velocity: float, accel: float):
        self.set_target_position_no_wait(position, velocity, accel)
        return struct.unpack('=B', self.cobs.read())

    def set_position(self, position: float):
        msg = struct.pack('=Bf', MSG_SET_POSITION, position * self.spu)
        self.cobs.write(msg)
        return struct.unpack('=B', self.cobs.read())

    def set_current(self, current: float):
        msg = struct.pack('=Bf', MSG_SET_CURRENT, current)
        self.cobs.write(msg)
        return struct.unpack('=B', self.cobs.read())

    def get_states(self):
        self.cobs.write(bytes([MSG_GET_STATES]))
        reply = self.cobs.read()
        # print("states returns: ", [byte for byte in reply])
        # print(reply)
        ack, position, velocity, acceleration, limit = struct.unpack('=BfffB', reply)

        return {
            'ack': ack,
            'position': position / self.spu,
            'velocity': velocity / self.spu,
            'acceleration': acceleration / self.spu,
            'limit': limit
        }


class HBridge:
    def __init__(self, port: str):
        self.cobs = CobsUsbSerial(port)

    def get_id(self):
        msg = struct.pack('=B', MSG_GET_ID)
        self.cobs.write(msg)
        ack, device_id = struct.unpack('=BB', self.cobs.read())
        return device_id

    def test(self) -> None:
        test_bytes = bytes([MSG_ECHO_TEST, 14, 16])
        print("testing w/ an echo-packet: ", [byte for byte in test_bytes])
        self.cobs.write(test_bytes)
        reply = self.cobs.read()
        print("test echo returns: ", [byte for byte in reply])

    def set_power(self, current: float, index: int):
        msg = struct.pack('=BfB', MSG_SET_POWER, current, index)
        self.cobs.write(msg)
        return struct.unpack('=B', self.cobs.read())

    def get_states(self):
        self.cobs.write(bytes([MSG_GET_STATES]))
        reply = self.cobs.read()
        # print("states returns: ", [byte for byte in reply])
        # print(reply)
        ack, power_a, power_b = struct.unpack('=Bff', reply)

        return {
            'ack': ack,
            'power_a': power_a,
            'power_b': power_b,
        }
