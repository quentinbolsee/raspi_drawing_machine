#include "COBSUSBSerial.h"
#include "serializationUtes.h"

// no-transport-layer dual H-Bridge
// using the RP2040 at 200MHz 
// using https://modular-things.github.io/modular-things/things/stepper-hbridge-xiao/

#define MSG_GET_ID        7
#define MSG_ECHO_TEST     11 
#define MSG_SET_POWER     20
#define MSG_GET_STATES    25
#define MSG_ACK           31 
#define MSG_NACK          32

#define AIN1_PIN 6      // on D4
#define AIN2_PIN 7      // on D5 
#define BIN1_PIN 28     // on D2
#define BIN2_PIN 4      // on D9 
#define APWM_PIN 27     // on D1
#define BPWM_PIN 29     // on D3 

// y_motor: 1, x_bottom_motor: 2, x_top_motor: 3 
#define MOTOR_ID 4

COBSUSBSerial cobs(&Serial);

float power_A = 0.f;
float power_B = 0.f;

void setPower(uint8_t* data, size_t len){
  // should do maxAccel, maxVel, and (optionally) setPosition
  // upstream should've though of this, so,
  uint16_t rptr = 0;
  float power = ts_readFloat32(data, &rptr);
  uint8_t i = data[rptr];

  if (power < 0.f) {
    power = 0.f;
  }
  if (power > 1.f) {
    power = 1.f;
  }

  if (i==0) {
    power_A = power;
  } else {
    power_B = power;
  }
  
  updateOutputs();
}


void updateOutputs() {
  if (power_A == 0.f) {
    digitalWrite(AIN1_PIN, LOW);
    digitalWrite(AIN2_PIN, LOW);
  } else {
    digitalWrite(AIN1_PIN, HIGH);
    digitalWrite(AIN2_PIN, LOW);
    analogWrite(APWM_PIN, int(255*power_A));
  }
  if (power_B == 0.f) {
    digitalWrite(BIN1_PIN, LOW);
    digitalWrite(BIN2_PIN, LOW);
  } else {
    digitalWrite(BIN1_PIN, HIGH);
    digitalWrite(BIN2_PIN, LOW);
    analogWrite(BPWM_PIN, int(255*power_B));
  }
}


size_t getStates(uint8_t* data, size_t len, uint8_t* reply){
  uint16_t wptr = 0;
  // in-fill current posn, velocity, and acceleration
  ts_writeFloat32(power_A, reply, &wptr);
  ts_writeFloat32(power_B, reply, &wptr);
  // return the data length 
  return wptr;
}

void setup() {
  pinMode(AIN1_PIN, OUTPUT);
  pinMode(AIN2_PIN, OUTPUT);
  pinMode(BIN1_PIN, OUTPUT);
  pinMode(BIN2_PIN, OUTPUT);
  pinMode(APWM_PIN, OUTPUT);
  pinMode(BPWM_PIN, OUTPUT);
  // and our comms
  cobs.begin();
}

uint8_t msg[256];
uint8_t reply[256];

void loop() {
  // run comms
  cobs.loop();
  // and check for paquiats: condition is if we can 
  // get a packet, and tx the reply
  if(cobs.clearToRead() && cobs.clearToSend()){
    size_t len = cobs.getPacket(msg);
    // start a reply, 
    size_t wptr = 0;
    reply[wptr ++] = MSG_ACK;
    // switch on grams,
    switch(msg[0]){
      case MSG_GET_ID:
        reply[wptr ++] = MOTOR_ID;
        break;
      case MSG_ECHO_TEST:
        memcpy(reply, msg, len);
        wptr = len;
        break;
      case MSG_SET_POWER:
        setPower(msg + 1, len - 1);
        break;
      case MSG_GET_STATES:
        wptr += getStates(msg + 1, len - 1, reply + 1);
        break;
      default:
        reply[0] = MSG_NACK;
        break;
    }
    // send the reply, 
    cobs.send(reply, wptr);
  }
}
