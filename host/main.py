# import the opencv library
import cv2
import numpy as np
import json
import time
import datetime
import filtering
import machine

STATE_INIT = 0
STATE_STREAM = 1
STATE_COUNTDOWN = 2
STATE_SHOWING = 3
STATE_VECTORIZED = 4
STATE_DRAWING = 5
STATE_DONE = 6


class MainApplication:
    def __init__(self, fullscreen=False, rotate=False):
        self.state = STATE_INIT
        self.window = cv2.namedWindow(
            "main", cv2.WINDOW_FULLSCREEN if fullscreen else cv2.WINDOW_KEEPRATIO)
        self.video = cv2.VideoCapture(0)
        self.width = 480
        self.height = 800
        self.img_captured = np.zeros(
            (self.height, self.width, 3), dtype=np.uint8)
        self.img = np.zeros((self.height, self.width, 3), dtype=np.uint8)
        self.click = None
        self.running = True
        self.overlays = {}
        self.rotate = rotate
        self.t_state = 0
        self.dt_state = 0
        self.strokes = None
        self.machine = None
        cv2.imshow("main", self.img)
        cv2.setMouseCallback("main", self.on_click)

    def load_overlays(self, config_overlays):
        for img_name in config_overlays:
            img_path = config_overlays[img_name]
            img = cv2.imread(img_path, cv2.IMREAD_UNCHANGED)
            img = img.astype(np.float32) / 255.0
            self.overlays[img_name] = img

    def run(self):
        while self.running:
            self.update()

    def on_click(self, event, x, y, flags, param):
        if event == cv2.EVENT_LBUTTONDOWN:
            # print(x, y, event)
            if self.rotate:
                self.click = (y, x)
            else:
                self.click = (x, self.height-y)

    def capture(self):
        ret, img = self.video.read()
        h, w, _ = img.shape
        w_crop = int(h * self.width / self.height)
        dw = w - w_crop
        img = img[:, dw // 2:-dw // 2, :]
        img = img[:, ::-1]
        self.img_captured = cv2.resize(img, (self.width, self.height))

    def update(self):
        previous_state = self.state
        overlay = "none"
        if self.state == STATE_INIT:
            self.state = STATE_STREAM
        elif self.state == STATE_STREAM:
            self.capture()
            self.img = self.img_captured
            overlay = "camera"
            if self.click is not None:
                self.click = None
                self.state = STATE_COUNTDOWN
        elif self.state == STATE_COUNTDOWN:
            self.capture()
            self.img = self.img_captured
            if self.dt_state <= 1:
                overlay = "3"
            elif self.dt_state <= 2:
                overlay = "2"
            elif self.dt_state <= 3:
                overlay = "1"
            else:
                self.state = STATE_SHOWING
        elif self.state == STATE_SHOWING:
            if self.dt_state < 0.1:
                self.img = 255 * \
                    np.ones((self.height, self.width), dtype=np.uint8)
            else:
                self.img = np.copy(self.img_captured)
                overlay = "yes_no"
                if self.click is not None:
                    x, y = self.click
                    self.click = None
                    if x < self.width // 2:
                        self.state = STATE_VECTORIZED
                    else:
                        self.state = STATE_STREAM
        elif self.state == STATE_VECTORIZED:
            if self.dt_state < 0.5:
                self.img = np.zeros(
                    (self.height, self.width, 3), dtype=np.uint8)
                overlay = "processing"
            else:
                if self.strokes is None:
                    polys = filtering.get_polylines(self.img_captured)
                    img_draw = 255 * \
                        np.ones((self.height, self.width, 3), dtype=np.uint8)
                    filtering.draw_polylines(img_draw, polys)
                    self.strokes = filtering.scale_offset_polylines(polys,
                                                                    self.width,
                                                                    self.height,
                                                                    100,
                                                                    machine.origin)
                    self.img = np.copy(img_draw)
                overlay = "yes_no"
                if self.click is not None:
                    x, y = self.click
                    self.click = None
                    if x < self.width // 2:
                        self.state = STATE_DRAWING
                    else:
                        self.strokes = None
                        self.state = STATE_STREAM
        elif self.state == STATE_DRAWING:
            if self.dt_state < 0.5:
                self.img = np.zeros((self.height, self.width, 3), np.uint8)
                overlay = "plotting"
            else:
                if self.strokes is not None and self.machine is not None:
                    self.machine.goto(0, machine.origin[1])
                    self.machine.goto(machine.origin[0], machine.origin[1])
                    for line in self.strokes:
                        self.machine.goto(line[0, 0], line[0, 1])
                        self.machine.pen_action(2, True)
                        self.machine.goto_list(line[:, 0], line[:, 1])
                        self.machine.pen_action(2, False)
                    self.machine.goto(machine.origin[0], machine.origin[1])
                    self.machine.goto(0, machine.origin[1])
                    self.machine.goto(0, 0)
                self.strokes = None
                self.state = STATE_STREAM

        self.show(overlay=overlay)
        if self.state != previous_state:
            self.t_state = datetime.datetime.now()
            self.dt_state = 0
        else:
            self.dt_state = (datetime.datetime.now() -
                             self.t_state).total_seconds()

    def show(self, overlay="none"):
        img_show = self.img
        if overlay != "none" and overlay in self.overlays:
            img_over = self.overlays[overlay]
            img_over_col = img_over[:, :, :3]
            alpha = img_over[:, :, 3:4]
            img_float = img_show.astype(np.float32) / 255.0
            img_show = np.clip(img_over_col * alpha +
                               img_float * (1-alpha), 0, 1)
        if self.rotate:
            cv2.imshow("main", cv2.rotate(img_show, cv2.ROTATE_90_CLOCKWISE))
        else:
            cv2.imshow("main", img_show)
        k = cv2.waitKey(10) & 0XFF
        if k == 27:
            # escape
            self.running = False


def main():
    with open("config.json", "r") as f:
        config = json.load(f)

    app = MainApplication(
        fullscreen=config["fullscreen"], rotate=config["rotate"])
    if config["use_machine"]:
        m = machine.Machine(config["ports"])
        m.pen_action(2, False)
        m.home()
        app.machine = m
    app.load_overlays(config["overlays"])
    app.run()
    app.video.release()
    cv2.destroyAllWindows()


if __name__ == "__main__":
    main()
