from motor import Motor, HBridge
import numpy as np
import math
import json
import serial.tools.list_ports
import time

home_rate = 60
home_accel = 2000
home_backoff_time = 0.2

go_rate = 180
go_accel = 2000

use_current = 0.4
origin = (130, 118)


class Machine:
    def __init__(self, ports_named):
        ports_found = find_ports(ports_named)
        self.solenoid = HBridge(ports_found["solenoid"])
        self.y_motor = Motor(ports_found["y_motor"])
        self.x_bottom_motor = Motor(ports_found["x_bot_motor"])
        self.x_top_motor = Motor(ports_found["x_top_motor"])

        # steps / unit,
        self.y_motor.set_steps_per_unit(200 / 40)
        self.x_bottom_motor.set_steps_per_unit(200 / 40)
        self.x_top_motor.set_steps_per_unit(200 / 40)

        # power up
        self.y_motor.set_current(use_current)
        self.x_bottom_motor.set_current(use_current)
        self.x_top_motor.set_current(use_current)

    def home(self):
        # home X ...
        if self.x_bottom_motor.get_states()['limit'] > 0:
            self.x_bottom_motor.set_target_velocity(home_rate, home_accel)
            self.x_top_motor.set_target_velocity(home_rate, home_accel)
            time.sleep(home_backoff_time)
            self.x_bottom_motor.set_target_velocity(0, home_accel)
            self.x_top_motor.set_target_velocity(0, home_accel)
            time.sleep(home_backoff_time)

        self.x_bottom_motor.set_target_velocity(-home_rate, home_accel)
        self.x_top_motor.set_target_velocity(-home_rate, home_accel)

        while True:
            states = self.x_bottom_motor.get_states()
            if states['limit'] > 0:
                break

        self.x_bottom_motor.set_target_velocity(0, home_accel)
        self.x_top_motor.set_target_velocity(0, home_accel)
        time.sleep(home_backoff_time)

        # home Y ....
        # if we're on the switch, backoff:
        if self.y_motor.get_states()['limit'] > 0:
            self.y_motor.set_target_velocity(home_rate, home_accel)
            time.sleep(home_backoff_time)
            self.y_motor.set_target_velocity(0, home_accel)
            time.sleep(home_backoff_time)

        # let's home it,
        self.y_motor.set_target_velocity(-home_rate, home_accel)

        # then check-wait on states,
        while True:
            states = self.y_motor.get_states()
            if states['limit'] > 0:
                break

        self.y_motor.set_target_velocity(0, home_accel)

        # set zeroes,
        self.x_bottom_motor.set_position(0)
        self.x_top_motor.set_position(0)
        self.y_motor.set_position(0)

    def pen_action(self, index=0, down=False, delay=0.05, power=0.6):
        if index == 2:
            self.solenoid.set_power(power if down else 0.0, 0)
            self.solenoid.set_power(power if down else 0.0, 1)
        else:
            self.solenoid.set_power(power if down else 0.0, index)
        time.sleep(delay)

    def goto(self, x, y, vel=None, accel=None, eps=0.3):
        if vel is None:
            vel = go_rate
        if accel is None:
            accel = go_accel
        self.x_bottom_motor.set_target_position(x, vel, accel)
        self.x_top_motor.set_target_position(x, vel, accel)
        self.y_motor.set_target_position(y, vel, accel)
        # ... wait while not at point:
        while True:
            x_states = self.x_top_motor.get_states()
            y_states = self.y_motor.get_states()
            x_err = abs(x_states["position"] - x)
            y_err = abs(y_states["position"] - y)
            if x_err <= eps and y_err <= eps:
                break

    def goto_list(self, x_list, y_list, eps=0.5):
        for x, y in zip(x_list, y_list):
            self.goto(x, y, eps=eps)


def find_ports(named_serial_numbers):
    named_serial_ports = {}
    ports = serial.tools.list_ports.comports()
    for name, serial_number in named_serial_numbers.items():
        for port in ports:
            if serial_number == port.serial_number:
                named_serial_ports[name] = port.device
    return named_serial_ports


if __name__ == "__main__":
    with open("config.json", "r") as f:
        config = json.load(f)
    m = Machine(config["ports"])
    m.home()
    m.goto(0, origin[1])
    m.goto(origin[0], origin[1])

    # w = 35
    # h = 35
    #
    # m.goto(origin[0]+w, origin[1]+h)
    # m.pen_action(2, True)
    # m.goto(origin[0]-w, origin[1]+h)
    # m.goto(origin[0]-w, origin[1]-h)
    # m.goto(origin[0]+w, origin[1]-h)
    # m.goto(origin[0]+w, origin[1]+h)
    # m.pen_action(2, False)
    # m.goto(0, origin[1])
    # m.goto(0, 0)

    import pickle

    with open("test/polys_np_keep.obj", "rb") as f:
        data = pickle.load(f)

    for line, _ in data:
        m.goto(line[0, 0], line[0, 1])
        m.pen_action(2, True)
        m.goto_list(line[:, 0], line[:, 1])
        m.pen_action(2, False)
        # for x, y in line:
        #     print(x, y)

    m.goto(origin[0], origin[1])
    m.goto(0, origin[1])
    m.goto(0, 0)

    # print(data)
