import cv2
import numpy as np
try:
    import trace_skeleton
    use_swig = True
except ImportError:
    import trace_skeleton_old
    use_swig = False
import skimage.morphology
import random


def bf(img):

    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    gray = cv2.equalizeHist(gray)
    filtered = cv2.bilateralFilter(gray, d=2, sigmaColor=10, sigmaSpace=10)
    # filtered = cv2.Sobel(filtered, ddepth=cv2.CV_8U, dx=1, dy=1, ksize=)
    # gray = cv2.cvtColor(filtered, cv2.COLOR_BGR2GRAY)
    edges = cv2.adaptiveThreshold(
        filtered, 250, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, blockSize=13, C=4)
    # post_filtered = cv2.bilateralFilter(edges, d=9, sigmaColor=75, sigmaSpace=75)
    edges = ~edges

    edges = cv2.dilate(edges, cv2.getStructuringElement(
        cv2.MORPH_ELLIPSE, (3, 3)))

    # return post_filtered
    return edges


def increase_bbox_size(bbox, pct=0.1):
    """
    Increase the bounding box by a percentage of its size.

    Parameters:
    - bbox: The bounding box in the format (x, y, width, height)
    - pct: The percentage by which to increase the bounding box size

    Returns:
    - A new bounding box increased by the given percentage
    """
    x, y, w, h = bbox
    x_new = int(x - pct * w)
    y_new = int(y - pct * h)
    w_new = int(w * (1 + 2 * pct))
    h_new = int(h * (1 + 2 * pct))
    return (x_new, y_new, w_new, h_new)


def get_polylines(img, min_length=5.0, n_max=200):

    face_cascade = cv2.CascadeClassifier(
        cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')
    faces = face_cascade.detectMultiScale(
        img, scaleFactor=1.8, minNeighbors=5, minSize=(50, 50))

    faces = [increase_bbox_size(face, 0.1) for face in faces]

    # Create a mask where the faces are 0 and the rest is 1
    height, width, _ = img.shape
    mask = np.zeros((height, width), dtype=bool)  # White mask
    for (x, y, w, h) in faces:
        mask[y:y+h, x:x+w] = True  # Set the face region to black

    img2 = bf(img)
    img_float = img2.astype(np.float32) / 255.0
    img_binary = img_float > 0.5

    im = skimage.morphology.skeletonize(img_binary).astype(np.uint8)

    if use_swig:
        polys = trace_skeleton.from_numpy(im)
    else:
        rects = []
        polys = trace_skeleton_old.traceSkeleton(
            im, 0, 0, im.shape[1], im.shape[0], 10, 999, rects)

    polys = [np.array(x, dtype=np.float32) for x in polys]

    polys_length_keep = []
    for line in polys:
        deltas = line[1:] - line[:-1]
        length = np.sum(np.linalg.norm(deltas, axis=1))

        if length >= min_length:
            all_x = line[:, 0].astype(np.int32)
            all_y = line[:, 1].astype(np.int32)
            all_x = np.clip(all_x, 0, width-1)
            all_y = np.clip(all_y, 0, height-1)
            if np.any(mask[all_y, all_x]):
                polys_length_keep.append([line, 5*length])
            else:
                polys_length_keep.append([line, length])

    polys_length_keep = sorted(polys_length_keep, key=lambda x: x[1])[::-1]
    polys_length_keep = polys_length_keep[:n_max]

    polys_keep = [x[0] for x in polys_length_keep]
    return polys_keep


def draw_polylines(img, polys):
    for l in polys:
        for i in range(0, len(l) - 1):
            cv2.line(img, (int(l[i][0]), int(l[i][1])), (int(
                l[i + 1][0]), int(l[i + 1][1])), (0, 0, 0), 2)


def scale_offset_polylines(polys, img_w, img_h, mm_w, offset):
    scale = mm_w / img_w

    polys_new = [np.copy(x) for x in polys]

    polys_new = sorted(polys_new, key=lambda x: -x[0, 1])

    for i in range(len(polys)):
        # flip
        polys_new[i][:, 1] = img_h - polys_new[i][:, 1]
        # zero center
        polys_new[i][:, 0] -= img_w/2
        polys_new[i][:, 1] -= img_h/2
        # scale to mm
        polys_new[i][:, :] *= scale

        # offset
        polys_new[i][:, 0] += offset[0]
        polys_new[i][:, 1] += offset[1]

    return polys_new


if __name__ == "__main__":
    # img = cv2.imread("old/test.png", cv2.IMREAD_UNCHANGED)
    # img = img[:, :, :3]
    # img2 = bf(img)
    # contours = stroke(img2)
    # plt.figure()
    # for cnt in contours:
    #     plt.plot(cnt[:, 0], cnt[:, 1])
    # plt.show()
    # # print(contours[0])
    img = cv2.imread("test/test.png", cv2.IMREAD_UNCHANGED)
    img = img[:, :, :3]
    h, w, _ = img.shape
    # img_copy = np.copy(cv2.cvtColor(img, cv2.COLOR_BGR2GRAY))*0
    polys = get_polylines(img)
    img_draw = 255*np.ones((h, w, 3), dtype=np.uint8)
    draw_polylines(img_draw, polys)

    lines = scale_offset_polylines(polys, w, h, 100, (130, 118))

    import matplotlib.pyplot as plt

    plt.figure()
    plt.imshow(img_draw)

    plt.figure()
    for xy in lines:
        plt.plot(xy[:, 0], xy[:, 1])
    plt.axis("equal")
    plt.show()

    # import pickle
    # with open("test/polys.obj", "wb") as f:
    #     pickle.dump(polys, f)

    # cv2.imshow("test", img_stroke)
    # cv2.waitKey(0)
