import pickle
import cv2
import numpy as np
import matplotlib.pyplot as plt


def process_polylines(polylines, img_w, img_h, mm_w, offset):
    polys_np_keep = []
    for line in polylines:
        deltas = line[1:] - line[:-1]
        length = np.sum(np.linalg.norm(deltas, axis=1))
        if length > 5.0:
            polys_np_keep.append([line, length])

    polys_np_keep = sorted(polys_np_keep, key=lambda x : x[1])[::-1]

    n_max = 200

    scale = mm_w / img_w

    polys_np_keep = polys_np_keep[:n_max]

    for i in range(len(polys_np_keep)):
        # flip
        polys_np_keep[i][0][:, 1] = img_h - polys_np_keep[i][0][:, 1]
        # zero center
        polys_np_keep[i][0][:, 0] -= img_w/2
        polys_np_keep[i][0][:, 1] -= img_h/2
        # scale to mm
        polys_np_keep[i][0][:, :] *= scale
        polys_np_keep[i][1] *= scale

        # offset
        polys_np_keep[i][0][:, 0] += offset[0]
        polys_np_keep[i][0][:, 1] += offset[1]

    return polys_np_keep

"""
def main():
    with open("polys.obj", "rb") as f:
        polys = pickle.load(f)

    img = cv2.imread("test.png", cv2.IMREAD_UNCHANGED)
    polys_np = [np.array(x, dtype=np.float32) for x in polys]
    print(polys_np[0].shape)
    polys_np_keep = []
    # lengths = []
    for line in polys_np:
        deltas = line[1:] - line[:-1]
        length = np.sum(np.linalg.norm(deltas, axis=1))
        if length > 5.0:
            polys_np_keep.append([line, length])

    polys_np_keep = sorted(polys_np_keep, key=lambda x : x[1])[::-1]

    n_max = 200

    polys_np_keep = polys_np_keep[:n_max]

    h, w, _ = img.shape
    # for line in

    print(w, h)

    # mm per px
    scale = 100 / w
    offset = (130, 118)
    # offset = (0, 0)

    for i in range(len(polys_np_keep)):
        # flip
        polys_np_keep[i][0][:, 1] = h - polys_np_keep[i][0][:, 1]
        # zero center
        polys_np_keep[i][0][:, 0] -= w/2
        polys_np_keep[i][0][:, 1] -= h/2
        # scale to mm
        polys_np_keep[i][0][:, :] *= scale
        polys_np_keep[i][1] *= scale

        # offset
        polys_np_keep[i][0][:, 0] += offset[0]
        polys_np_keep[i][0][:, 1] += offset[1]

    # for line, length in polys_np_keep:
    #     plt.plot(line[:, 0], -line[:, 1])
    #
    # # print(polys_np_keep[:50])
    plt.figure()
    for line, _ in polys_np_keep:
        plt.plot(line[:, 0], line[:, 1])
    # plt.hist(lengths, bins=np.linspace(0, 160, 64))
    plt.axis("equal")
    plt.show()

    polys_np_keep = sorted(polys_np_keep, key=lambda x: x[0][0, 1])

    with open("polys_np_keep.obj", "wb") as f:
        pickle.dump(polys_np_keep, f)
"""


# if __name__ == "__main__":
#     main()
